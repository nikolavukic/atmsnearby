// general variables

var lattitude;
var longitude;
var currpos;
var placeposition;
var atms;
var atmsresults;
var atmslist = $('#atms-list').get(0);
var staticmap = $('#static-map').get(0);
var staticmarkers;
var distance;
var multicurrencyflag = false;
var reverseflag = false;
var locationoptions = {
	timeout: 5000,
	maximumAge: 500000
};

function getLocationSuccess(position){
	lattitude = position.coords.latitude;
	longitude = position.coords.longitude;
	currpos = new google.maps.LatLng(lattitude, longitude);
	getAtmsList();
};

function getLocationError(error){
	switch(error.code) {
		case error.PERMISSION_DENIED:
			showNotification('You blocked geo location information request. Refresh the page and allow geo location in order to use <strong>ATMs Nearby</strong>');
			break;
		case error.POSITION_UNAVAILABLE:
			showNotification('Your geo location information request is not available.');
			break;
		case error.TIMEOUT:
			showNotification('It seems your geo location request timed out. Please refresh the page and try again.');
			break;
		case error.UNKNOWN_ERROR:
			showNotification('An unknown error occurred.');
			break;
	}
};

function getLocation(){
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(
			getLocationSuccess, getLocationError, locationoptions
		);
	} else {
		showNotification('It seems your browser does not support geo location.');
	};
};

function getAtmsList(){
	atms = new google.maps.places.PlacesService($('#atms-helper').get(0));
	atms.nearbySearch({
		location: {"lat" : lattitude, "lng" : longitude},
		type: ['atm'],
		rankBy: google.maps.places.RankBy.DISTANCE
	}, processResults);
};

function processResults(results, status){
	if (status !== google.maps.places.PlacesServiceStatus.OK) {
		showNotification("We couldn't retrieve nearby ATMs information.");
		return;
	} else {
		atmsresults = results;
		populateAtmsList(atmsresults);
	};
};

function populateAtmsList(places){
	atmslist.innerHTML = '';
	staticmarkers = '';
	var j = 0;
	if (reverseflag === false){
		for (var i = 0, place; place = places[i]; i++) {
			var color = 'color:0x19c539%7C';
			var tstring = /telenor/;
			var lowerpname = place.name.toLowerCase();
			if ((multicurrencyflag === true) && (tstring.test(lowerpname) !== true)) {
				continue;
			};
			if ((tstring.test(lowerpname) === true)){
				color = 'color:0x1698fb%7C';
			}
			placeposition = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
			distance = google.maps.geometry.spherical.computeDistanceBetween(currpos, place.geometry.location);
			atmslist.innerHTML += '<li><h3>' + place.name + '<span> - ' + distance.toFixed(1) + ' m' + '</span></h3></li>';
			staticmarkers += '&markers=' + color + parseFloat(place.geometry.location.lat().toFixed(4)) + ',' + parseFloat(place.geometry.location.lng().toFixed(4));
			if (j == 9) {
				break; // I am aware that this is not the most efficient way of limiting the list
			};
			j++;
		}
	} else {
		for (var i=places.length-1; place = places[i]; i--) {
			var color = 'color:0x19c539%7C';
			var tstring = /telenor/;
			var lowerpname = place.name.toLowerCase();
			if ((multicurrencyflag === true) && (tstring.test(lowerpname) !== true)) {
				continue;
			};
			if ((tstring.test(lowerpname) === true)){
				color = 'color:0x1698fb%7C';
			}
			placeposition = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
			distance = google.maps.geometry.spherical.computeDistanceBetween(currpos, place.geometry.location);
			atmslist.innerHTML += '<li><h3>' + place.name + '<span> - ' + distance.toFixed(1) + ' m' + '</span></h3></li>';
			staticmarkers += '&markers=' + color + parseFloat(place.geometry.location.lat().toFixed(4)) + ',' + parseFloat(place.geometry.location.lng().toFixed(4));
			if (j == 9) {
				break; // I am aware that this is not the most efficient way of limiting the list
			};
			j++;
		}
	};
	createStaticMap();
	hideWelcome();
};

function createStaticMap(){
	staticmap.src = 'https://maps.googleapis.com/maps/api/staticmap?zoom=16&key=AIzaSyAT1E_3bFXWZU94seYoP-gvEsf9ZFQ_JY4&size=640x640&center=' + lattitude + ',' + longitude + staticmarkers + '&markers=color:0xc31c1c%7C' + lattitude + ',' + longitude;
	staticmap.style.display = 'block';
};

function changeSorting(){
	var btt1 = document.getElementById('asc');
	var btt2 = document.getElementById('desc');
	if (btt1.style.display === 'none') {
		btt1.style.display = 'inline';
	} else {
		btt1.style.display = 'none';
	};
	if (btt2.style.display === 'none') {
		btt2.style.display = 'inline';
	} else {
		btt2.style.display = 'none';
	};
	if (reverseflag === true) {
		reverseflag = false;
	} else {
		reverseflag = true;
	};
	populateAtmsList(atmsresults);
};

function toggleFilter(){
	var btt1 = document.getElementById('all');
	var btt2 = document.getElementById('mc-only');
	if (btt1.style.display === 'none') {
		btt1.style.display = 'inline';
	} else {
		btt1.style.display = 'none';
	};
	if (btt2.style.display === 'none') {
		btt2.style.display = 'inline';
	} else {
		btt2.style.display = 'none';
	};
	if (multicurrencyflag === true) {
		multicurrencyflag = false;
	} else {
		multicurrencyflag = true;
	};
	populateAtmsList(atmsresults);
};

function hideWelcome(){
	$('#welcome-wrapper').get(0).className += ' hidden';
}

function showNotification(text){
	$('#notice-text').get(0).innerHTML = text;
	$('#welcome-wrapper').get(0).classList.remove('hidden');
}

// Function Calls

$(document).ready(function(){
	
	getLocation();
	
})